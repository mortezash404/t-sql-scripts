-- نمایش لیست دیتابیس ها
EXEC sp_helpdb

-- نمایش لیست دیتا فایل ها و لاگ فایل ها
USE BTS
EXEC sp_helpfile

-- نمایش لیست تمام جداول یک دیتابیس
SELECT * FROM sys.tables

-- نمایش حجم دیتابیس
EXEC sp_spaceused

-- نمایش حجم و تعداد رکورد یک جدول
EXEC sp_spaceused BtsInfoes

--نمایش تمام ایندکس های یک جدول
EXEC sp_helpindex Roles





