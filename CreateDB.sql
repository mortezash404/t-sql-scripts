--ایجاد یک بانک اطلاعاتی با چند فایل
USE master
GO
--بررسی 
IF DB_ID('Test01')>0
	DROP DATABASE Test01
GO
CREATE DATABASE Test01
	ON
	(
		NAME=DataFile1,FILENAME='C:\Dump\DataFile1.mdf',
		SIZE=10MB,MAXSIZE=200MB,FILEGROWTH=10%
	),
	(
		NAME=DataFile2,FILENAME='C:\Dump\DataFile2.ndf',
		SIZE=20MB,MAXSIZE=300MB,FILEGROWTH=10%
	),
	(
		NAME=DataFile3,FILENAME='C:\Dump\DataFile3.ndf',
		SIZE=30MB,MAXSIZE=400MB,FILEGROWTH=10%
	)
	LOG ON
	(
		NAME=LogFile1,FILENAME='C:\Dump\LogFile1.ldf',
		SIZE=100MB,MAXSIZE=5GB,FILEGROWTH=1024MB
	),
	(
		NAME=LogFile2,FILENAME='C:\Dump\LogFile2.ldf',
		SIZE=200MB,MAXSIZE=5GB,FILEGROWTH=1024MB
	)
GO